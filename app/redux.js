import { applyMiddleware, combineReducers, createStore } from 'redux'

import thunk from 'redux-thunk'

// actions.js
export const storeValue = val => ({
    type: 'STORE_VAL',
    val
})

export const toggleStudent = student => ({
    type: 'TOGGLE_STUDENT',
    student
})

export const toggleNi = ni => ({
    type: 'TOGGLE_NI',
    ni
})

export const emptyValue = () => ({
    type: 'EMPTY_VAL'
})

// reducers.js
export const val = (state = {}, action) => {
    switch (action.type) {
        case 'STORE_VAL':
            return action.val
        case 'EMPTY_VAL':
            return {}
        default:
            return state
    }
}

export const student = (state = {}, action) => {
    switch (action.type) {
        case 'TOGGLE_STUDENT':
            return action.student
        default:
            return state
    }
}

export const ni = (state = {}, action) => {
    switch (action.type) {
        case 'TOGGLE_NI':
            return action.ni
        default:
            return state
    }
}


export const reducers = combineReducers({
    val,
    student,
    ni
})

// store.js
export function configureStore(initialState = {}) {
    const store = createStore(reducers, initialState, applyMiddleware(thunk))
    return store
}

export const store = configureStore()
