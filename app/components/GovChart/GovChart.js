import React, { Component } from 'react'

import PieChart from 'react-minimal-pie-chart'

const GovChart = () => {
    const one = '#a1292f'
    const two = '#d4974c'
    const three = '#8aa58a'
    const four = '#9ba596'
    const five = '#6d0032'
    const six = '#71292f'
    const seven = '#94974c'
    const eight = '#13a54a'
    const nine = '#2ba596'
    const ten = '#1d2032'

    return (
        <div className="chart-container">
        
            <div className="half-col">
                <PieChart
                    data={[
                        { value: 161, key: 1, color: one },
                        { value: 147, key: 2, color: two },
                        { value: 86, key: 3, color: three },
                        { value: 45, key: 4, color: four },
                        { value: 114, key: 5, color: five },
                        { value: 29, key: 6, color: six },
                        { value: 32, key: 7, color: seven },
                        { value: 18, key: 8, color: eight },
                        { value: 124, key: 9, color: nine },
                        { value: 57, key: 10, color: ten }

                    ]}
                />
            </div>

            <div className="half-col">

                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: one }} />
                    <p>Public Pensions - £161B</p>
                </div>

                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: two }} />
                    <p>National Health Care - £147B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: three }} />
                    <p>Defence - £86B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: four }} />
                    <p>Social Security - £45B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: five }} />
                    <p>State Protection - £144B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: six }} />
                    <p>Transport - £29B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: seven }} />
                    <p>State Protection - £32B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: eight }} />
                    <p>General Government - £18B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: nine }} />
                    <p>Other Public Services - £124B</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: ten }} />
                    <p>Public Sector Interest - £57B</p>
                </div>

            </div>
        </div>
    )
}

export default GovChart
