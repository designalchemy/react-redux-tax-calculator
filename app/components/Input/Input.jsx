import React, { Component } from 'react'

import { connect } from 'react-redux'

import { storeValue, toggleStudent, toggleNi } from '../../redux'

import Toggle from 'react-toggle'

import style from './Input.scss'

export const Input = props => (
    <div>
        <p>Yearly Wage</p>

        <input type="number" onChange={e => props.storeValue({ inputValue: e.target.value })} />

        <p>Student Loan</p>

        <Toggle onChange={e => props.toggleStudent({ student: e.target.checked })} />

        <p>Self Employed</p>

        <Toggle onChange={e => props.toggleNi({ ni: e.target.checked })} />

        {/*
            <p>Tax Year</p>

            <select>
                <option>2017/18</option>
                <option>2016/17</option>
                <option>2015/16</option>
            </select>
        */}
    </div>
)

const mapStateToProps = (state, ownProps) => ({
    value: state.val,
    student: state.student
})

const mapDispatchToProps = {
    storeValue,
    toggleStudent,
    toggleNi
}

const conn = connect(mapStateToProps, mapDispatchToProps)(Input)

export default conn
