import React, { Component } from 'react'

import PieChart from 'react-minimal-pie-chart'

import { connect } from 'react-redux'

import {
    calcYear,
    calcTax,
    calcNi,
    calcStudent,
    calcNiClassTwo,
    totalDeductions,
    totalEarnings
} from '../../utils'

import { configureStore } from '../../redux'

import style from './Chart.scss'

export const Chart = props => {
    let { inputValue } = props.value
    const { student } = props.student
    const { ni } = props.ni

    inputValue = inputValue && inputValue > 0 ? Number(inputValue) : 1
    inputValue = inputValue < 999999999 ? Number(inputValue) : 999999999

    const studentVal = student ? inputValue : 0
    const niVal = ni ? inputValue : 0

    const green = '#06AD09'
    const orange = '#F79704'
    const red = '#F73F04'
    const yellow = '#EDE309'
    const otherColor = '#ED09E6'

    return (
        <div className="chart-container">

            <div className="half-col">
                <PieChart
                    data={[
                        { value: inputValue, key: 1, color: green },
                        { value: calcTax(inputValue), key: 2, color: red },
                        { value: calcNi(!ni ? inputValue : 0), key: 3, color: orange },
                        { value: calcNiClassTwo(niVal), key: 4, color: otherColor },
                        { value: calcStudent(studentVal), key: 5, color: yellow }
                    ]}
                />
            </div>

            <div className="half-col">

                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: green }} />
                    <p>Your Wage</p>
                </div>

                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: red }} />
                    <p>Income Tax</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: orange }} />
                    <p>National Insurance</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: otherColor }} />
                    <p>National Insurance class 2 + 4</p>
                </div>


                <div className="chart-row">
                    <div className="chart-row__color" style={{ backgroundColor: yellow }} />
                    <p>Student Loan</p>
                </div>

            </div>
        </div>
    )
}

Chart.defaultProps = {
    value: { inputValue: 1 }
}

const mapStateToProps = (state, ownProps) => ({
    value: state.val,
    student: state.student,
    ni: state.ni
})

const conn = connect(mapStateToProps, {})(Chart)

export default conn
