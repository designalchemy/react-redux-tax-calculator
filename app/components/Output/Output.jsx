import React, { Component } from 'react'

import { connect } from 'react-redux'

import { configureStore } from '../../redux'

import {
    calcYear,
    calcMonth,
    calcWeek,
    calcDay,
    calcHour,
    calcTaxFree,
    calcTax,
    calcDeductible,
    calcNi,
    calcStudent,
    calcNiClassTwo,
    totalDeductions,
    totalEarnings
} from '../../utils'

import style from './Output.scss'

export const Output = props => {
    let { inputValue } = props.value
    const { student } = props.student
    const { ni } = props.ni

    inputValue = inputValue && inputValue > 0 ? inputValue : 0
    inputValue = inputValue < 999999999 ? inputValue : 999999999

    const data = {
        total: inputValue,
        tax: calcTax(inputValue),
        ni: calcNi(inputValue),
        ni2: calcNiClassTwo(inputValue),
        student: calcStudent(inputValue)
    }

    return (
        <div>
            <div className="output-table__container">
                <div className="row row-header">
                    <div className="cell">&nbsp;</div>
                    <div className="cell">
                        <em>Yearly</em>
                    </div>
                    <div className="cell">
                        <em>Monthly</em>
                    </div>
                    <div className="cell">
                        <em>Weekly</em>
                    </div>
                    <div className="cell">
                        <em>Daily</em>
                    </div>
                    <div className="cell">
                        <em>Hourly</em>
                    </div>
                </div>

                <div className="row">
                    <div className="cell">
                        <em>Gross</em>
                    </div>
                    <div className="cell">£{calcYear(inputValue)}</div>
                    <div className="cell">£{calcMonth(inputValue)}</div>
                    <div className="cell">£{calcWeek(inputValue)}</div>
                    <div className="cell">£{calcDay(inputValue)}</div>
                    <div className="cell">£{calcHour(inputValue)}</div>
                </div>

                <div className="row">
                    <div className="cell">
                        <em>Tax Free</em>
                    </div>
                    <div className="cell">£{calcYear(calcTaxFree(inputValue))}</div>
                    <div className="cell">£{calcMonth(calcTaxFree(inputValue))}</div>
                    <div className="cell">£{calcWeek(calcTaxFree(inputValue))}</div>
                    <div className="cell">£{calcDay(calcTaxFree(inputValue))}</div>
                    <div className="cell">£{calcHour(calcTaxFree(inputValue))}</div>
                </div>

                <div className="row">
                    <div className="cell">
                        <em>Tax Deductible</em>
                    </div>
                    <div className="cell">£{calcYear(calcDeductible(inputValue))}</div>
                    <div className="cell">£{calcMonth(calcDeductible(inputValue))}</div>
                    <div className="cell">£{calcWeek(calcDeductible(inputValue))}</div>
                    <div className="cell">£{calcDay(calcDeductible(inputValue))}</div>
                    <div className="cell">£{calcHour(calcDeductible(inputValue))}</div>
                </div>

                <div className="row">
                    <div className="cell">
                        <em>Tax Due</em>
                    </div>
                    <div className="cell">£{calcYear(calcTax(inputValue))}</div>
                    <div className="cell">£{calcMonth(calcTax(inputValue))}</div>
                    <div className="cell">£{calcWeek(calcTax(inputValue))}</div>
                    <div className="cell">£{calcDay(calcTax(inputValue))}</div>
                    <div className="cell">£{calcHour(calcTax(inputValue))}</div>
                </div>

                {!ni && (
                    <div className="row">
                        <div className="cell">
                            <em>NI Due</em>
                        </div>
                        <div className="cell">£{calcYear(calcNi(inputValue))}</div>
                        <div className="cell">£{calcMonth(calcNi(inputValue))}</div>
                        <div className="cell">£{calcWeek(calcNi(inputValue))}</div>
                        <div className="cell">£{calcDay(calcNi(inputValue))}</div>
                        <div className="cell">£{calcHour(calcNi(inputValue))}</div>
                    </div>
                )}

                {ni && (
                    <div className="row">
                        <div className="cell">
                            <em>Class 2 + 4 NI</em>
                        </div>
                        <div className="cell">£{calcYear(calcNiClassTwo(inputValue))}</div>
                        <div className="cell">£{calcMonth(calcNiClassTwo(inputValue))}</div>
                        <div className="cell">£{calcWeek(calcNiClassTwo(inputValue))}</div>
                        <div className="cell">£{calcDay(calcNiClassTwo(inputValue))}</div>
                        <div className="cell">£{calcHour(calcNiClassTwo(inputValue))}</div>
                    </div>
                )}

                {student && (
                    <div className="row">
                        <div className="cell">
                            <em>Stuent Loan</em>
                        </div>
                        <div className="cell">£{calcYear(calcStudent(inputValue))}</div>
                        <div className="cell">£{calcMonth(calcStudent(inputValue))}</div>
                        <div className="cell">£{calcWeek(calcStudent(inputValue))}</div>
                        <div className="cell">£{calcDay(calcStudent(inputValue))}</div>
                        <div className="cell">£{calcHour(calcStudent(inputValue))}</div>
                    </div>
                )}

                <div className="row red">
                    <div className="cell">
                        <em>Total Deductions</em>
                    </div>
                    <div className="cell">
                        £{calcYear(totalDeductions(inputValue, student, ni))}
                    </div>
                    <div className="cell">
                        £{calcMonth(totalDeductions(inputValue, student, ni))}
                    </div>
                    <div className="cell">
                        £{calcWeek(totalDeductions(inputValue, student, ni))}
                    </div>
                    <div className="cell">£{calcDay(totalDeductions(inputValue, student, ni))}</div>
                    <div className="cell">
                        £{calcHour(totalDeductions(inputValue, student, ni))}
                    </div>
                </div>

                <div className="row green">
                    <div className="cell">
                        <em>Total Earnings</em>
                    </div>
                    <div className="cell">£{calcYear(totalEarnings(inputValue, student, ni))}</div>
                    <div className="cell">£{calcMonth(totalEarnings(inputValue, student, ni))}</div>
                    <div className="cell">£{calcWeek(totalEarnings(inputValue, student, ni))}</div>
                    <div className="cell">£{calcDay(totalEarnings(inputValue, student, ni))}</div>
                    <div className="cell">£{calcHour(totalEarnings(inputValue, student, ni))}</div>
                </div>
            </div>
        </div>
    )
}

Output.defaultProps = {
    value: { inputValue: 0 }
}

const mapStateToProps = (state, ownProps) => ({
    value: state.val,
    student: state.student,
    ni: state.ni
})

const conn = connect(mapStateToProps, {})(Output)

export default conn
