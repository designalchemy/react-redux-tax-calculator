import React, { Component } from 'react'
import { connect } from 'redux'

import Input from '../Input/Input'
import Output from '../Output/Output'
import Chart from '../Chart/Chart'
import GovChart from '../GovChart/GovChart'

import style from './Main.scss'

const Main = () => (
    <div>

        <header>

            <div className="container">
                <h1>Simple UK Tax Calcuator - 2017/2018</h1>
            </div>

        </header>

        <div className="container">

            <div className="main-container__flex">

                <div className="input-container">
                    <Input />
                </div>

                <div className="output-container">
                    <Output />
                </div>

            </div>

            <div className="main-container__flex" style={{ marginTop: '60px' }}>

                <div className="output-container">
                    <h1 className="text-sub__header">Tax Breakdown</h1>

                    <Chart />
                </div>

                <div className="output-container">
                    <h1 className="text-sub__header">Uk Tax Spending</h1>

                    <GovChart />
                </div>

            </div>

        </div>

        <footer>

            <div className="container">
                &copy; 2017 simple-uk-tax.co.uk - by{' '}
                <a href="http://designalchemy.co.uk">DesignAlchemy.co.uk</a>
            </div>
            
        </footer>

    </div>
)

export default Main
