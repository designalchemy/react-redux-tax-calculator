import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'

import { Provider } from 'react-redux';  
import { store } from './redux'

import App from './App'
import Main from './components/Main/Main'

const browserHistory = createBrowserHistory()

const Routes = () => {
    return (
        <Provider store={store}>
            <Router history={browserHistory}>
                <Switch>
                    <Route exact path="/no-container" component={Main} />

                    <App>
                        <Switch>
                            <Route exact path="/" component={Main} />
                            <Route component={Main} render={() => {

                                <Main />
                            }} /> {/* not found route */}
                        </Switch>
                    </App>
                </Switch>
            </Router>
        </Provider>
    )
}

export default Routes
